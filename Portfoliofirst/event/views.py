from django.shortcuts import render
from .models import Eventin

def event(request):
    event = Eventin.objects.all()
    return render(request, 'portfolio/event.html', {'event':event})

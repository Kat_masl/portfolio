from django.db import models
from project.models import Projects

class Eventin(models.Model):
    title = models.ManyToManyField(to=Projects, related_name='ev')
    date = models.DateField('Дата')
    description = models.TextField('description')
    name_project = models.CharField('Название проекта', max_length=80)
    result = models.PositiveSmallIntegerField('Результат', null=True, blank=True)

    def __str__(self):#Вывод объекта
        return self.description

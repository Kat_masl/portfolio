from django.db import models
from portfolio.models import Student

class Groupin(models.Model):
    nikname = models.ManyToManyField(to=Student, related_name='nik')
    group = models.CharField('Группа', max_length=600)
    name_facultet = models.TextField('Факультет')
    name_spec = models.TextField('Специальность')

    def __str__(self):#Вывод объекта
        return self.group

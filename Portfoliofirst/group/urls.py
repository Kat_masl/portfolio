from django.urls import path
from . import views #Подключить файл views

urlpatterns = [
    path('', views.groups, name='groups'),
    path('<int:id_group>', views.group, name='group')
]
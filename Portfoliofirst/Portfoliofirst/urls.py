from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('portfolio.urls')),#Переход на главную страничку
    path('project/', include('project.urls')),
    path('univer/', include('univer.urls')),
    path('load/', include('load.urls')),
    path('group/', include('group.urls')),
    path('event/', include('event.urls')),
]

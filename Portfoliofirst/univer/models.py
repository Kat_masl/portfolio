from django.db import models
from portfolio.models import Student

class Univer(models.Model):
    nikname = models.ManyToManyField(to=Student, related_name='Имя')
    universitet = models.CharField('Университет', max_length=150, null=True)
    description = models.TextField('description')
    address = models.CharField('Адрес', max_length=150)

    def __str__(self):#Вывод объекта
        return self.universitet

from django.urls import path
from . import views #Подключить файл views

urlpatterns = [
    path('', views.univers, name='univers'),
    path('<int:id_univer>', views.univer, name='univer'),
]
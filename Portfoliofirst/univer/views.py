from django.shortcuts import render, get_object_or_404
from .models import Univer

def univers(request):
    univers = Univer.objects.all()
    return render(request, 'portfolio/univer.html', {'univers':univers})

def univer(request, id_univer: int):
    univer = get_object_or_404(Univer,id=id_univer)
    return render(request, 'portfolio/unever1.html', {'univer':univer})


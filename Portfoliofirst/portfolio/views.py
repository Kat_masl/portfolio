from django.shortcuts import render, redirect
from .forms import StudentForm
from .models import Student
from .forms2 import ProjectsForm
from project.models import Projects
from django.http import HttpResponseRedirect
from django.http import HttpResponseNotFound
from django.views.generic import UpdateView

def index(request):
    error = ''
    if request.method == 'POST':
        form = StudentForm(request.POST)#Проверка данных из формы
        if form.is_valid():
            form.save()#Если они корректны то мы сохраняем данные в базу данных
            return redirect('main')#Перееадресация на сайт
        else: 
            error = 'Форма была не верной'

    form = StudentForm()

    data = {
        'form': form,#Ключ форм
        'error': error
    }

    return render(request, 'portfolio/index.html', data)

def main(request):
    main = Student.objects.all()   
    return render(request, 'portfolio/main.html', {'main':main})

def acaunt(request):
    acaunt = Student.objects.all().order_by('-id')[:1]
    return render(request, 'portfolio/acaunt.html', {'acaunt':acaunt})

def forms(request):
    error = ''
    if request.method == 'POST':
        forms = ProjectsForm(request.POST)#Проверка данных из формы
        if forms.is_valid():
            forms.save()#Если они корректны то мы сохраняем данные в базу данных
            return redirect('pro')#Перееадресация на сайт
        else: 
            error = 'Форма была не верной'

    forms = ProjectsForm()

    data = {
        'forms': forms,#Ключ форм
        'error': error
    }

    return render(request, 'portfolio/forms.html', data)

def pro(request):
    pro = Projects.objects.all().order_by('-id')[:1]
    return render(request, 'portfolio/pro.html', {'pro':pro})

# изменение данных в бд
def update(request, id):
    try:
        pro = Projects.objects.get(id=id)
 
        if request.method == "POST":
            pro.project = request.POST.get("project")
            pro.description = request.POST.get("description")
            pro.file_project = request.POST.get("file_project")
            pro.file_document = request.POST.get("file_document")
            pro.event = request.POST.get("event")
            pro.result = request.POST.get("result")
            pro.save()
            return HttpResponseRedirect("pro")
        else:
            return render(request, "portfolio/update.html", {"pro": pro})
    except Projects.DoesNotExist:
        return HttpResponseNotFound("<h2>Нет информации</h2>")


# удаление данных из бд
def delete(request, id):
    try:
        pro = Projects.objects.get(id=id)
        pro.delete()
        return redirect('acaunt')#Перееадресация на сайт
    except Projects.DoesNotExist:
        return HttpResponseNotFound("<h2>Нет информации</h2>")

def search_venues(request):
	if request.method == "POST":
		searched = request.POST['searched']
		venues = Projects.objects.filter(project__contains=searched)
	
		return render(request, 
		'portfolio/search_events.html', 
		{'searched':searched,
		'venues':venues, })
	else:
		return render(request, 
		'portfolio/search_events.html', 
		{})



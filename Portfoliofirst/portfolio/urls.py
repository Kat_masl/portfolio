from django.urls import path
from . import views #Подключить файл views

urlpatterns = [
    path('', views.index),
    path('main', views.main, name='main'),
    path('acaunt', views.acaunt, name='acaunt'),
    path('forms', views.forms, name='forms'),
    path('pro', views.pro, name='pro'),
    path('update/<int:id>/', views.update, name='update'),
    path('delete/<int:id>/', views.delete),
    path('search_venues', views.search_venues, name='search-venues'),
] 


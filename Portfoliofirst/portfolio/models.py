from django.db import models

class Student(models.Model):
    name = models.CharField('Имя', max_length=20, null=True)
    sname = models.CharField('Фамилия', max_length=30, null=True)
    lname = models.CharField('Отчество', max_length=30)
    email = models.EmailField('Email')
    password = models.CharField('Пароль', max_length=50)
    universitet = models.CharField('Университет', max_length=50)
    group = models.CharField('Группа', max_length=600)
    project = models.CharField('Проект' , max_length=50)
    load = models.CharField('Нагрузка' , max_length=500)

    def __str__(self):#Вывод объекта
        return self.name + ' ' + self.sname




        

from .models import Student
from django.forms import ModelForm, TextInput

class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ['name', 'sname', 'lname', 'email', 'password', 'universitet', 'group', 'project', 'load']

        widgets ={
            'name': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите свое имя'
            }),
            'sname': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите свою фамилию'
            }),
            'lname': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите свое отчество'
            }),
            'email': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'you@example.com',
            }),
            'password': TextInput(attrs={
                'class': 'form-control'
            }),
            'universitet': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите свой университет'
            }),
            'group': TextInput(attrs={
                'class': 'form-control'
            }),
            'project': TextInput(attrs={
                'class': 'form-control'
            }),
            'load': TextInput(attrs={
                'class': 'form-control'
            }),
        }


from project.models import Projects
from django.forms import ModelForm, TextInput, Textarea

class ProjectsForm(ModelForm):
    class Meta:
        model = Projects
        fields = ['project', 'description', 'file_project', 'file_document', 'event', 'result']

        widgets ={
            'project': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите название проекта'
            }),
            'description': Textarea(attrs={
                'class': 'form-control',
                'rows': '3',
                'placeholder': 'Введите описание проекта'
            }),
            'file_project': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Добавьте url проекта'
            }),
            'file_document': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Добавьте url документации проекта',
            }),
            'event': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите название конкурса'
            }),
            'result': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Ожидаемый результат',
            }),
        }
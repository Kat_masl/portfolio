
from project.models import Projects
from django.forms import ModelForm, TextInput, Textarea

class ProjectsForm(ModelForm):
    class Meta:
        model = Projects
        fields = ['project', 'description', 'file_project', 'file_document', 'event', 'result']

        widgets ={
            'project': TextInput(attrs={
                'class': 'form-control',
                'value':'{{pro.project}}'
            }),
            'description': Textarea(attrs={
                'class': 'form-control',
                'rows': '3',
                'value':'{{pro.description}}'
            }),
            'file_project': TextInput(attrs={
                'class': 'form-control',
                'value':'{{pro.file_project}}'
            }),
            'file_document': TextInput(attrs={
                'class': 'form-control',
                'value':'{{pro.file_document}}'
            }),
            'event': TextInput(attrs={
                'class': 'form-control',
                'value':'{{pro.event}}'
            }),
            'result': TextInput(attrs={
                'class': 'form-control',
                'value':'{{pro.result}}'
            }),
        }
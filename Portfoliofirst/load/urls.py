from django.urls import path
from . import views #Подключить файл views

urlpatterns = [
    path('', views.loads, name='loads'),
    path('<int:id_load>', views.load, name='load')
]
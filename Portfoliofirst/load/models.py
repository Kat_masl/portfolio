from django.db import models
from portfolio.models import Student

class Loadin(models.Model):
    nikname = models.ManyToManyField(to=Student, related_name='Никнейм')
    semestr = models.CharField('Семестр', max_length=100)
    form_education = models.CharField('Форма обучения', max_length=100)
    load = models.CharField('Нагрузка' , max_length=50)

    def __str__(self):#Вывод объекта
        return self.semestr

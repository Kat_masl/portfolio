from ast import Load
from django.shortcuts import render, get_object_or_404
from .models import Loadin

def loads(request): 
    loads = Loadin.objects.all()
    return render(request, 'portfolio/load.html', {'loads':loads})

def load(request, id_load: int):
    load = get_object_or_404(Loadin,id=id_load)
    return render(request, 'portfolio/load1.html', {'load':load})